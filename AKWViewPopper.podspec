Pod::Spec.new do |s|
  s.name     = 'AKWViewPopper'
  s.version  = '1.0.0'
  s.platform = :ios, '6.0'
  s.license  = 'MIT' 
  s.summary  = 'UIView popup presentation library, with custom animations and background views'
  s.homepage = 'https://bitbucket.org/arkorwan/akwviewpopper'
  s.author   = { 'Worakarn Isaratham' => 'arkorwan@gmail.com' }
  s.source   = { :git => 'https://bitbucket.org/arkorwan/akwviewpopper.git', :tag => 'v1.0.0' }
  s.description = 'UIView popup presentation library, with custom animations and background views'
  s.source_files = 'AKWViewPopper/*.{h,m}'
  s.requires_arc = true
end
