//
//  AKWViewPopper.m
//  FaceSong
//
//  Created by toon on 6/9/14.
//  Copyright (c) 2014 arkorwan. All rights reserved.
//

#import "AKWViewPopper.h"

@interface AKWViewPopper ()

@property (strong, nonatomic) UIView *background;
@property (weak, nonatomic) UIView *currentManagedView;
@property (assign, nonatomic) BOOL isDismissing;

@end

@implementation AKWViewPopper

-(instancetype) init
{
    if(self = [super init]){
        self.shouldDismissOnBackgroundTap = YES;
        self.showInterval = 0.4;
        self.dismissInterval = 0.4;
        self.transtionStyle = AKWTransitionPopUp;
        self.isDismissing = NO;
    }
    return self;
}

-(void) prepareBackgroundViewForSuperview:(UIView *) superview
{
    CGRect frame = (CGRect){{0, 0}, superview.frame.size};
    if(self.backgroundViewCreator){
        self.background = self.backgroundViewCreator(superview);
        self.background.frame = frame;
    } else {
        self.background = [[UIView alloc] initWithFrame:frame];
        self.background.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5];
    }
    if(self.shouldDismissOnBackgroundTap){
        UITapGestureRecognizer *tapper = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onBackgroundTapped:)];
        [self.background addGestureRecognizer:tapper];
    }
    self.background.alpha = 0;
    [superview addSubview:self.background];
}

-(CGAffineTransform) slideTransform
{
    CGFloat superViewWidth = self.currentManagedView.superview.frame.size.width;
    CGFloat superViewHeight = self.currentManagedView.superview.frame.size.height;
    
    switch(self.transtionStyle){
        case AKWTransitionSlideInFromTop:
            return CGAffineTransformMakeTranslation(0, -CGRectGetMaxY(self.currentManagedView.frame));
        case AKWTransitionSlideInFromLeft:
            return CGAffineTransformMakeTranslation(-CGRectGetMaxX(self.currentManagedView.frame), 0);
        case AKWTransitionSlideInFromBottom:
            return CGAffineTransformMakeTranslation(0, superViewHeight - CGRectGetMinY(self.currentManagedView.frame));
        case AKWTransitionSlideInFromRight:
            return CGAffineTransformMakeTranslation(superViewWidth - CGRectGetMinX(self.currentManagedView.frame), 0);
        default: //use right
            return CGAffineTransformMakeTranslation(superViewWidth - CGRectGetMinX(self.currentManagedView.frame), 0);
    }
}

-(void) animatePopupWithCompletion:(void(^)(void)) completion
{
    NSTimeInterval interval = self.showInterval;
    if(interval > 0){
        [UIView animateWithDuration:interval/2 animations:^{
            self.background.alpha = 0.5;
            self.currentManagedView.transform = CGAffineTransformMakeScale(1.05, 1.05);
        } completion:^(BOOL finished){
            [UIView animateWithDuration:interval/4 animations:^{
                self.background.alpha = 1;
                self.currentManagedView.transform = CGAffineTransformMakeScale(0.95, 0.95);
            } completion:^(BOOL finished){
                [UIView animateWithDuration:interval/4 animations:^{
                    self.currentManagedView.transform = CGAffineTransformIdentity;
                } completion:^(BOOL finished) {
                    if(completion){
                        completion();
                    }
                }];
            }];
        }];
    } else {
        self.background.alpha = 1;
        self.currentManagedView.transform = CGAffineTransformIdentity;
        if(completion){
            completion();
        }
    }
}

-(void) animatePopupDismissWithCompletion:(void(^)(BOOL finished)) completion
{
    NSTimeInterval interval = self.dismissInterval;
    if(interval > 0){
        [UIView animateWithDuration:interval/3 animations:^{
            self.background.alpha = 0.66;
            self.currentManagedView.transform = CGAffineTransformMakeScale(1.05, 1.05);
        } completion:^(BOOL finished){
            [UIView animateWithDuration:2*interval/3 animations:^{
                self.background.alpha = 0;
                self.currentManagedView.alpha = 0.0;
                self.currentManagedView.transform = CGAffineTransformMakeScale(0.5, 0.5);
            } completion:completion];
        }];
    } else {
        self.background.alpha = 0;
        self.currentManagedView.alpha = 0.0;
        completion(YES);
    }
}

-(void) animateSlideInWithCompletion:(void(^)(void)) completion
{
    self.currentManagedView.transform = [self slideTransform];
    NSTimeInterval interval = self.showInterval;
    if(interval > 0){
        [UIView animateWithDuration:interval animations:^{
            self.background.alpha = 1;
            [self.currentManagedView setTransform:CGAffineTransformIdentity];
        } completion:^(BOOL finished) {
            if(completion){
                completion();
            }
        }];
    } else {
        self.background.alpha = 1;
        [self.currentManagedView setTransform:CGAffineTransformIdentity];
        if(completion){
            completion();
        }
    }
}

-(void) animateSlideInDismissWithCompletion:(void(^)(BOOL finished)) completion
{
    
    NSTimeInterval interval = self.dismissInterval;
    if(interval > 0){
        [UIView animateWithDuration:interval animations:^{
            self.background.alpha = 0;
            self.currentManagedView.transform = [self slideTransform];
        } completion:completion];
    } else {
        completion(YES);
    }
}


-(void) popupView:(UIView *) view inView:(UIView *) superview completion:(void(^)(void)) completion
{
    CGFloat xInset = (superview.frame.size.width - view.frame.size.width)/2;
    CGFloat yInset = (superview.frame.size.height - view.frame.size.height)/2;
    [self popupView:view inView:superview withInsets:UIEdgeInsetsMake(yInset, xInset, yInset, xInset) completion:completion];
}


-(void) popupView:(UIView *) view inView:(UIView *) superview withInsets:(UIEdgeInsets) insets completion:(void(^)(void)) completion
{
    if(self.currentManagedView){
        return;
    }
    
    self.currentManagedView = view;
    [self prepareBackgroundViewForSuperview:superview];
    
    CGFloat superViewWidth = superview.frame.size.width;
    CGFloat superViewHeight = superview.frame.size.height;
    
    view.frame = CGRectMake(insets.left, insets.top, superViewWidth - insets.left - insets.right, superViewHeight - insets.top - insets.bottom);
    
    [superview addSubview:view];
    
    switch (self.transtionStyle) {
        case AKWTransitionPopUp:
            [self animatePopupWithCompletion:completion];
            break;
        case AKWTransitionSlideInFromTop:
        case AKWTransitionSlideInFromLeft:
        case AKWTransitionSlideInFromBottom:
        case AKWTransitionSlideInFromRight:
            [self animateSlideInWithCompletion:completion];
            break;
    }
}

-(void) onBackgroundTapped:(id) recognizer
{
    UIView *managedView = self.currentManagedView;
    [self dismissCurrentViewWithCompletion:^{
        if(self.completionOnBackgroundTapDismissal){
            self.completionOnBackgroundTapDismissal(managedView);
        }
    }];
}

-(void) dismissCurrentViewWithCompletion:(void(^)(void)) completion
{
    if(self.isDismissing){
        return;
    }
    
    self.isDismissing = YES;
    void(^internalCompletion)(BOOL) = ^(BOOL finished){
        self.currentManagedView.alpha = 1;
        self.currentManagedView.transform = CGAffineTransformIdentity;
        [self.currentManagedView removeFromSuperview];
        self.currentManagedView = nil;
        [self.background removeFromSuperview];
        self.background = nil;
        self.isDismissing = NO;
        if(completion){
            completion();
        }
    };
    
    switch (self.transtionStyle) {
        case AKWTransitionPopUp:
            [self animatePopupDismissWithCompletion:internalCompletion];
            break;
        case AKWTransitionSlideInFromTop:
        case AKWTransitionSlideInFromLeft:
        case AKWTransitionSlideInFromBottom:
        case AKWTransitionSlideInFromRight:
            [self animateSlideInDismissWithCompletion:internalCompletion];
            break;
    }
}

@end
