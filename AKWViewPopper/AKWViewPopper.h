//
//  AKWViewPopper.h
//  FaceSong
//
//  Created by toon on 6/9/14.
//  Copyright (c) 2014 arkorwan. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef UIView* (^UIViewTranform)(UIView *view);

typedef NS_ENUM(NSUInteger, AKWViewTransitionStyle){
    AKWTransitionPopUp,
    AKWTransitionSlideInFromTop,
    AKWTransitionSlideInFromLeft,
    AKWTransitionSlideInFromBottom,
    AKWTransitionSlideInFromRight
};

@interface AKWViewPopper : NSObject

@property (copy) UIViewTranform backgroundViewCreator;
@property (copy) void (^completionOnBackgroundTapDismissal)(UIView *managedView);
@property (assign, nonatomic) BOOL shouldDismissOnBackgroundTap;
@property (assign, nonatomic) NSTimeInterval showInterval;
@property (assign, nonatomic) NSTimeInterval dismissInterval;
@property (assign, nonatomic) AKWViewTransitionStyle transtionStyle;

@property (weak, nonatomic, readonly) UIView *currentManagedView;

-(void) popupView:(UIView *) view inView:(UIView *) superview completion:(void(^)(void)) completion;
-(void) popupView:(UIView *) view inView:(UIView *) superview withInsets:(UIEdgeInsets) insets completion:(void(^)(void)) completion;

-(void) dismissCurrentViewWithCompletion:(void(^)(void)) completion;

@end